import "ant-design-vue/dist/antd.css";
import "./assets/main.scss";


import { createApp } from "vue";
import App from "./App.vue";
import Antd from "ant-design-vue";
import ToastPlugin from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-bootstrap.css';


createApp(App).use(Antd).use(ToastPlugin).mount("#app");
